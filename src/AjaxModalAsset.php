<?php
namespace fafcms\assets\ajaxmodal;

use yii\web\AssetBundle;

class AjaxModalAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/assets';

    public $js = [
        'js/ajax-modal.js',
    ];

    public $depends = [
        'fafcms\assets\init\InitAsset',
    ];
}
