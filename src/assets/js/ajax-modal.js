;(function (factory) {
    'use strict'
    new factory(window.jQuery, window.fafcms)
})(function ($, fafcms) {
    'use strict'

    /***
     * Add events for specific modal by adding an object with your modal id based on the object
     * @type {{modalID: {openStart: Array, openEnd: Array, closeStart: Array, closeEnd: Array, contentLoaded: Array}}}
     */
    fafcms.prototype.modalEvents = {}

    fafcms.prototype.openModal = function(options) {
        try {
            $('#ajax-modal').modal('close')
        } catch(e) {}

        if (typeof options.modalEvents === 'undefined') {
            options.modalEvents = {
                openStart: [],
                openEnd: [],
                closeStart: [],
                closeEnd: [],
                contentLoaded: [],
            }

            if (typeof options.modalId !== 'undefined' && typeof fafcms.prototype.modalEvents[options.modalId] !== 'undefined') {
                options.modalEvents = {
                    openStart: fafcms.prototype.modalEvents[options.modalId].openStart,
                    openEnd: fafcms.prototype.modalEvents[options.modalId].openEnd,
                    closeStart: fafcms.prototype.modalEvents[options.modalId].closeStart,
                    closeEnd: fafcms.prototype.modalEvents[options.modalId].closeEnd,
                    contentLoaded: fafcms.prototype.modalEvents[options.modalId].contentLoaded
                }
            }
        }

        $('#ajax-modal').modal({
            opacity: .7,
            onOpenStart: function(e) {
                fafcms.prototype.runEvents(options.modalEvents, 'openStart', e)
            },
            onOpenEnd: function(e) {
                fafcms.prototype.runEvents(options.modalEvents, 'openEnd', e)
            },
            onCloseStart: function(e) {
                fafcms.prototype.runEvents(options.modalEvents, 'closeStart', e)
            },
            onCloseEnd: function(e) {
                if (typeof options.modalClass !== 'undefined') {
                    $('#ajax-modal').removeClass(options.modalClass)
                }

                fafcms.prototype.runEvents(options.modalEvents, 'closeEnd', e)
            }
        })

        $('#ajax-modal .modal-content').html(fafcms.prototype.loader)
        $('#ajax-modal .modal-footer').remove()
        $('#ajax-modal .modal-header').remove()
        $('#ajax-modal').removeClass('modal-fixed-footer')
        $('#ajax-modal').modal('open')

        $.get(options.url, options.data).done(function(response) {
            var content
            var footer
            var header

            if (typeof options.modalSelector !== 'undefined') {
                content = $('<div>' + response + '<div>').find(options.modalSelector).html()
            } else {
                content = response
            }

            if (typeof options.modalFixed !== 'undefined') {
                $('#ajax-modal').addClass('modal-fixed-footer')
            }

            if (typeof options.modalClass !== 'undefined') {
                $('#ajax-modal').addClass(options.modalClass)
            }

            if (typeof options.headerSelector !== 'undefined') {
                header = $('<div>' + response + '<div>').find(options.headerSelector).html()
            }

            if (typeof options.footerSelector !== 'undefined') {
                footer = $('<div>' + response + '<div>').find(options.footerSelector).html()
            }

            $('#ajax-modal .modal-content').html(content)

            if (typeof header !== 'undefined') {
                $('#ajax-modal').prepend('<header class="modal-header">' + header + '</header>')
            } else {
                $('#ajax-modal > .modal-header').remove()
            }

            if (typeof footer !== 'undefined') {
                $('#ajax-modal').append('<div class="modal-footer">' + footer + '</div>')
            } else {
                $('#ajax-modal > .modal-footer').remove()
            }

            if (typeof options.headerSelector !== 'undefined' && typeof options.headerRemoveFromContent !== 'undefined' && (options.headerRemoveFromContent === true || options.headerRemoveFromContent === 'true')) {
                $('#ajax-modal .modal-content').find(options.headerSelector).remove()
            }

            if (typeof options.footerSelector !== 'undefined' && typeof options.footerRemoveFromContent !== 'undefined' && (options.footerRemoveFromContent === true || options.footerRemoveFromContent === 'true')) {
                $('#ajax-modal .modal-content').find(options.footerSelector).remove()
            }

            $('#ajax-modal').trigger('magic-modal-content-change')

            if (!$('#ajax-modal').hasClass('open')) {
                $('#ajax-modal').modal('open')
            }

            fafcms.prototype.runEvents(fafcms.prototype.events, 'initPlugins', $('#ajax-modal .modal-content'))
            fafcms.prototype.runEvents(options.modalEvents, 'contentLoaded')
        }).fail(function (e) {
            if (typeof e.responseJSON !== 'undefined' && typeof e.responseJSON.message !== 'undefined') {
                if (e.status === 403 && e.responseJSON.code === 42) {
                    fafcms.prototype.openModal(JSON.parse(e.responseJSON.message))
                    return false
                }

                M.toast({
                    html: e.responseJSON.message,
                    classes: 'red'
                })
            } else {
                M.toast({
                    html: 'Es ist ein Fehler aufgetreten',
                    classes: 'red'
                })
            }

            $('#ajax-modal').modal('close')
        })
    }

    fafcms.prototype.events['init'].push(function () {
        if ($('#ajax-modal').length === 0) {
            $('body').append('<div id="ajax-modal" class="modal"><div class="modal-content"></div></div>')
        }

        $('body').on('click', '.ajax-modal', function(e) {
            e.preventDefault()

            var $ajaxModal = $(this)
            var url = $ajaxModal.attr('href')

            if (typeof url === 'undefined') {
                url = $ajaxModal.attr('xlink:href')
            }

            var modalSelector = $ajaxModal.data('ajax-modal-content')
            var headerSelector = $ajaxModal.data('ajax-modal-header')
            var headerRemoveFromContent = $ajaxModal.data('ajax-modal-header-remove')
            var footerSelector = $ajaxModal.data('ajax-modal-footer')
            var footerRemoveFromContent = $ajaxModal.data('ajax-modal-footer-remove')
            var modalFixed = $ajaxModal.data('ajax-modal-fixed')
            var modalId = $ajaxModal.data('ajax-modal-id')
            var modalData = $ajaxModal.data('ajax-modal-data')
            var modalUrl = $ajaxModal.data('ajax-modal-url')
            var modalClass = $ajaxModal.data('ajax-modal-class')

            if (typeof modalUrl !== 'undefined') {
                url = modalUrl
            }

            if (typeof modalData !== 'undefined') {
                modalData = JSON.parse(modalData)
            }

            if (typeof fafcms.prototype.modalEvents[modalId] === 'undefined') {
                fafcms.prototype.modalEvents[modalId] = {}
            }

            fafcms.prototype.openModal({
                url: url,
                data: modalData,
                modalSelector: modalSelector,
                headerSelector: headerSelector,
                headerRemoveFromContent: headerRemoveFromContent,
                footerSelector: footerSelector,
                footerRemoveFromContent: footerRemoveFromContent,
                modalFixed: modalFixed,
                modalId: modalId,
                modalClass: modalClass,
                modalEvents: {
                    openStart: fafcms.prototype.modalEvents[modalId].openStart,
                    openEnd: fafcms.prototype.modalEvents[modalId].openEnd,
                    closeStart: fafcms.prototype.modalEvents[modalId].closeStart,
                    closeEnd: fafcms.prototype.modalEvents[modalId].closeEnd,
                    contentLoaded: fafcms.prototype.modalEvents[modalId].contentLoaded
                }
            })
        })
    })

    return fafcms
})
